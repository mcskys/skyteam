package fr.skykits.team;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;

import fr.skykits.api.scoreboard.SkyBoard;
import fr.skykits.api.utils.messages.StartServerFormat;
import fr.skykits.team.command.CommandScoreboard;
import fr.skykits.team.command.CommandTeam;

public class SkyTeam extends JavaPlugin {

	
	@Override
    public void onEnable() {
		StartServerFormat.setStartMessage(this);
		
        // Register our command "kit" (set an instance of your command class as executor)
        this.getCommand("createteam").setExecutor(new CommandTeam());
        this.getCommand("scoreboard").setExecutor(new CommandScoreboard());
    }
	
	
	/**
	 * Set a refresh for all players of the scoreboard.
	 */
	public void scoreBoardAround() {
		Bukkit.getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {

			@Override
			public void run() {
				SkyBoard sky = SkyBoard.getInstance();
				sky.createScoreBoard("Toto", "Kills", "deathCount", DisplaySlot.PLAYER_LIST, "Kill");
			}
		}, 20, 1200);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] arg3) {
		if (sender instanceof Player) {
			
		} else {
			return false;
		}
		return true;
	}
}
