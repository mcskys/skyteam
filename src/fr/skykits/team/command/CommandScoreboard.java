package fr.skykits.team.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;

import fr.skykits.api.scoreboard.SkyBoard;

public class CommandScoreboard implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			if (cmd.getName().equals("scoreboard") && sender.hasPermission("skys.group.staff.bureau")) {
				if (args.length < 5) {
					Bukkit.broadcastMessage("La commande doit comporter 5 paramètres."); 
				} else {
					SkyBoard scoreBoard = SkyBoard.getInstance();
					scoreBoard.createScoreBoard(args[0], args[1], args[2], getSlot(args[3]), args[4]);
				}

			}

		} else {
			return false;
		}
		return true;
	}

	/**
	 * TODO : Put it in API
	 * Return the displaySlot
	 * @param slotName
	 * @return
	 */
	private DisplaySlot getSlot(String slotName) {
		return DisplaySlot.valueOf(slotName);
	}
}
